var spawnRate = 10,
    spawnRateOfDescent = 25,
    lastSpawn = 1,
    objects = [],
    startTime = Date.now(),
    canvas = document.getElementById("myCanvas"),
    ctx = canvas.getContext("2d"),
    theInterval=setInterval(draw, 25),
    audio = document.getElementById("audio");

//Func for random colors
function rg(m, n) {
    return Math.floor( Math.random() * (n - m + 1) ) + m;
}
function hex(i) {
    return i.toString(16);
}
function randColor() {
    return '#' + hex(rg(1, 15)) + hex(rg(1, 15)) + hex(rg(1, 15)) +
        hex(rg(1, 15)) + hex(rg(1, 15)) + hex(rg(1, 15));
}
//Start game button func
function startGame(){
    $('#button').addClass('hidden');
    $('#myCanvas').removeClass('hidden');
    $("#myCanvas").click('#myCanvas', function(){
        document.getElementById('audio').pause();
        alert('Animation paused, to continue just click "OK"!');
        document.getElementById('audio').play();
    });
    audio.play();
}
//Circles parameters
function spawnRandomObject() {
    var object = {
        type: randColor(),
        //Random top position
        x: Math.random() * canvas.width,
        y: 0,
        //Random Radius
        r: Math.floor(20 + (Math.random() * 50))
    };
    objects.push(object);
}

function animate() {
    var time = Date.now();
    if (time > (lastSpawn + spawnRate)) {
        lastSpawn = time;
        spawnRandomObject();
    }
    //Create circles loop
    for (var i = 0; i < objects.length; i++) {
        var object = objects[i];
        object.y += spawnRateOfDescent;
        ctx.beginPath();
        ctx.arc(object.x, object.y, object.r, 0, Math.PI * 2);
        ctx.closePath();
        ctx.fillStyle = object.type;
        ctx.fill();
    }
}

function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    animate();
}


